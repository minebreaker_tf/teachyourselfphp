<?php

function hello()
{
    return "hello, world!";
}

echo hello(), '<br/>';

function multiply($x, $y)
{
    return $x * $y;
}

echo '3 x 6 = ' . multiply(3, 6), '<br/>';

function add(...$args)
{
    if (empty($args)) {
        return 0;
    } else {
        return array_shift($args) + add(...$args); // IntelliJ can't detect this recursive call!!!
    }
}

echo add(1, 2, 3, 4, 5, 6), '<br/>';

$anonymous_function = function () {
    return 'hoge';
};
echo $anonymous_function(), '<br/>';
