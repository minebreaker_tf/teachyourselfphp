<?php

class Test
{
    public $public_var;
    private $private_var;
    private static $class_variable = 0;
    const HOGE = "hoge"; // Where's the hell define() gone?

    function __construct()
    {
        $this->public_var = 100;
        $this->nevermore();
    }

    function inc()
    {
        $this->private_var++;
    }

    function get_var()
    {
        return $this->private_var;
    }

    private function nevermore()
    {
        echo 'nevermore!', '<br/>';
        echo self::HOGE, '<br/>'; // self for static, this for instance
        echo self::class_method(), '<br/>';
    }

    private static function class_method()
    {
        return 'Static hello!';
    }

}

$class = new Test();
echo $class->public_var, '<br/>';
$class->inc();
echo $class->get_var(), '<br/>';

class SuperTest extends Test
{
    function __construct()
    {
        // if the constructor is defined, you must explicitly call parent constructor.
        parent::__construct();
    }

    function inc()
    {
        echo 'Override!!!';
    }

    private function nevermore()
    {
        echo 'Not override!';
    }

    function __set($name, $value)
    {
        echo 'You fucked it.', '<br/>';
    }

    function __call($name, $args)
    {
        echo 'You called non-exist function';
    }

    function __toString()
    {
        return 'Just like Java!';
    }
}

$class2 = new SuperTest();
$class2->inc();
echo $class2->get_var(), '<br/>';

/** @noinspection PhpUndefinedFieldInspection */
$class->nonexist_property = 'PHP automatically creates property when it doesn\'t exist!';
/** @noinspection PhpUndefinedFieldInspection */
echo $class->nonexist_property, '<br/>';
/** @noinspection PhpUndefinedMethodInspection */
//$class->nonexist_function(); Fatal error
/** @noinspection PhpUndefinedFieldInspection */
$class2->nonexist_property = 'Use __set() and __get() to prevent this behavior!';
/** @noinspection PhpUndefinedFieldInspection */
echo $class2->nonexist_property;
/** @noinspection PhpUndefinedMethodInspection */
$class2->nonexist_function();
