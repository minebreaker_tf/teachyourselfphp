<?php

// Closure

function accumulator()
{
    $val = 0;
    return function ($add) use (&$val) {
        $val += $add;
        return $val;
    };
}

$accum = accumulator();
echo $accum(1), '<br/>';
echo $accum(2), '<br/>';
echo $accum(3), '<br/>';


// Map Reduce

$arr = range(1, 10); // inclusive
$even = array_filter($arr, function ($n) {
    return $n % 2 == 0;
});
$pow = array_map(function ($n) {
    return $n * $n;
}, $even);
// use array_sum() in production!
$res = array_reduce($pow, function ($rest, $n) {
    return $rest + $n;
});
echo $res, '<br/>';
