<?php

if (TRUE) {
    echo 'if statement', '<br/>';
} else if (FALSE) {
    echo 'this will not be shown!', '<br/>';
} else {
    echo 'this will not be shown too.', '<br/>';
}

if (0) {
    echo '0 is true', '<br/>';
} else {
    echo '0 is false', '<br/>';
}

echo 'Equality:', '<br/>';
echo 1 == '1', '<br/>'; // true
echo 1 === '1', '<br/>'; // false
echo strcmp(1, '1'), '<br/>'; // 0 i.e. false

echo 'Operators:', '<br/>';
echo true && true || false, '<br/>'; // true
echo true && (true && false), '<br/>'; // false
echo true xor false, '<br/>'; // true
echo true && !false, '<br/>'; // true

function returnsTrue()
{
    echo 'Hello there!', '<br/>';
    return true;
}

if (true || returnsTrue()) {
    echo 'Short-circuit evaluation!', '<br/>';
}
if (true && returnsTrue()) {
    echo 'You\'ll see hello this time!', '<br/>';
}

switch (123) {
    case 0:
        echo 'The value is 0', '<br/>';
        break;
    case 123:
        echo 'That\'s it!', '<br/>';
        break;
    default:
        echo 'Everything goes wrong.', '<br/>';
}

echo TRUE ? 'Ternary operator!' : 'oops!';
