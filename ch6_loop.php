<?php

$i = 0;
while ($i < 4) {
    $i++;
    if ($i > 2) continue;
    echo "Iteration: " . $i, '<br/>';
}

while (true) {
    while (true) {
        break;
    }
    echo 'Breaks nearest loop by default.', '<br/>';
    break;
}

while (true) {
    while (true) {
        break 2;
    }
    echo 'Skipped!', '<br/>';
}
echo 'You escaped the loop!', '<br/>';

for ($i = 0, $j = 10; $i < 10, $j > 5; $i++, $j--) {
    echo "i = $i, j = $j", '<br/>';
}

foreach (array(1, 2, 3, 4, 5, 6) as $var) {
    echo $var, '<br/>';
}
