<?php

echo 123 + 456, '<br/>'; // 579
echo 01 + 07, '<br/>'; // 8
echo 0x1 + 0xf, '<br/>'; // 16

echo 3.14156, '<br/>';
echo 2e-5 - 0.00001, '<br/>';

echo 2 * 3, '<br/>';
echo 9 / 3, '<br/>';
echo 9 % 4, '<br/>';

echo 1 + 2.3, '<br/>';
/** @noinspection PhpWrongStringConcatenationInspection */
echo '123 autocast!!!' + ' 877', '<br/>';
/** @noinspection PhpWrongStringConcatenationInspection */
echo 'string is evaluated as zero' + 999, '<br/>';
