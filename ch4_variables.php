<?php

// uninitialized variables are evaluated as NULL
// or, notice is show if you don't set default value.
/** @noinspection PhpUndefinedVariableInspection */
echo $uninitialized, '<br/>';

$nil_value = null;
/** @noinspection PhpUndefinedVariableInspection */
echo $nil + 1, '<br/>'; // 1
echo $nil - 1, '<br/>'; // null

echo gettype(1), '<br/>';
echo gettype('1'), '<br/>';
echo gettype($nil), '<br/>';

$n = 123;
echo $n, '<br/>';
$n = 456;
echo $n, '<br/>';

$a = 123;
$b = 456;
echo "a = ${a}, b = ${b}", '<br/>';
$a = $b;
echo "a = ${a}, b = ${b}", '<br/>';
$b = 789;
echo "a = ${a}, b = ${b}", '<br/>';
$a =& $b;
echo "a = ${a}, b = ${b}", '<br/>';
$b = 123;
echo "a = ${a}, b = ${b}", '<br/>';

$con = 'abc';
$con .= 'def';
echo $con, '<br/>';

define('CONSTANT', 'This is constant value.');
echo CONSTANT, '<br/>';
