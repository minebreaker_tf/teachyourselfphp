<?php

$arr = array(1, 2, 3, 4, 5, 6);
echo $arr[0], '<br/>';
echo $arr[1], '<br/>';
$arr[2] = 100;
echo $arr[2], '<br/>';
print_r($arr);
echo '<br/>';

$assoc_arr = array('a' => 1, 'b' => 2, 'c' => 3);
echo $assoc_arr['a'], '<br/>';
print_r($assoc_arr);
echo '<br/>';

$auto_assoc = array();
$auto_assoc[] = 'a';
$auto_assoc[] = 'b';
$auto_assoc[100] = 'c';
$auto_assoc[] = 'd';
print_r($auto_assoc);
echo '<br/>';

$nd_arr = array(array('a', 'b', 'c'), array(1, 2, 3));
echo $nd_arr[0][0];
echo $nd_arr[0][1];
echo $nd_arr[1][0];
