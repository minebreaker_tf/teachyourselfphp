<?php

interface Greeter
{
    function hello();
}

class EnglishGreeter implements Greeter
{

    function hello()
    {
        return 'hello, world';
    }
}

abstract class AbstractJapaneseGreeter implements Greeter
{
    function hello()
    {
        return 'こんにちは世界';
    }
}

class JapaneseGreeter extends AbstractJapaneseGreeter
{
}

function echoGreet($greeter)
{
    echo $greeter->hello(), '<br/>';
}

trait JapaneseGreeterTrait
{
    function hello()
    {
        return 'こんにちは世界ってなんだよ';
    }
}

class NewJapaneseGreeter
{
    use JapaneseGreeterTrait;
}

echoGreet(new EnglishGreeter());
echoGreet(new JapaneseGreeter());
echoGreet(new NewJapaneseGreeter());
// You can use anonymous class in PHP 7!
//echoGreet(new class extends AbstractJapaneseGreeter
//{
//    function hello()
//    {
//        return 'ｵｯｽｵｯｽ';
//    }
//});
