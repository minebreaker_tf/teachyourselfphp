<!DOCTYPE html>
<html>
<body>
<ul>
    <?php
    $articles = array(
        'PSR-2' => 'http://www.infiniteloop.co.jp/docs/psr/psr-2-coding-style-guide.html',
        '@ operator' => 'http://stackoverflow.com/questions/1032161/what-is-the-use-of-the-symbol-in-php',
        'TRUE vs. true' => 'http://stackoverflow.com/questions/2013848/uppercase-booleans-vs-lowercase-in-php',
        'PHPでクロージャ―を使う' => 'http://qiita.com/hugo-sb/items/3e344486658e3cfbd407',
        'PHP 5.3 のラムダとクロージャーを活用する' => 'https://www.ibm.com/developerworks/jp/opensource/library/os-php-lambda/',

    );
    foreach (array_keys($articles) as $title) {
        echo '<li>';
        echo '<a href="' . $articles[$title] . '">' . $title . '</a>';
        echo '</li>';
    }
    ?>
</ul>
</body>
</html>
